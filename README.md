# VISPR-online
VISPR-online is a web-based interactive framework for CRISPR screens visualization.

Deployment
-------
### Step1:
Install Python 3. Create development environment with
```
$ pip install -r requirements.txt
```
### Setp2:
Start the application with
```
$ ./run.py
```

Author
------
Yingbo Cui <cuiyingbomail@163.com>

License
-------

Licensed under the MIT license (http://opensource.org/licenses/MIT). This project may not be copied, modified, or distributed except according to those terms.